module butcherbot

go 1.16

require (
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/pelletier/go-toml v1.9.0
	golang.org/x/oauth2 v0.0.0-20210427180440-81ed05c6b58c
)
