package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"github.com/pelletier/go-toml"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/oauth2/twitch"
)

type App struct {
	Config    Config      `toml:"config"`
	Reactions []Reaction  `toml:"reactions"`
	Scheduled []Scheduled `toml:"scheduled"`
}
type Config struct {
	User         string   `toml:"user"`
	Oauth        string   `toml:"oauth"`
	Channels     []string `toml:"channels"`
	InitDelay    int      `toml:"init_schedule_delay_seconds"`
	ClientId     string   `toml:"client_id"`
	ClientSecret string   `toml:"client_secret"`
	accessToken  string
}
type Reaction struct {
	TargetChannel string `toml:"target_channel"`
	Trigger       string `toml:"trigger"`
	TriggerUser   string `toml:"trigger_user"`
	Reaction      string `toml:"reaction"`
	DelaySeconds  int    `toml:"delay_seconds"`
}
type Scheduled struct {
	TargetChannel string `toml:"target_channel"`
	Action        string `toml:"action"`
	Interval      int    `toml:"interval"`
	ExecOnInit    bool   `toml:"exec_on_init"`
}

func main() {

	tasks := readTaskFile()

	oauth2Config := &clientcredentials.Config{
		ClientID:     tasks.Config.ClientId,
		ClientSecret: tasks.Config.ClientSecret,
		TokenURL:     twitch.Endpoint.TokenURL,
	}

	token, err := oauth2Config.Token(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	tasks.Config.accessToken = token.AccessToken

	tasks.connectToChat()

}

func (tasks *App) connectToChat() {
	client := twitchIRC.NewClient(tasks.Config.User, tasks.Config.Oauth)

	client.OnPrivateMessage(func(m twitchIRC.PrivateMessage) {
		for _, reaction := range tasks.Reactions {
			if reaction.react(m, client) {
				return
			}
		}
	})

	client.OnConnect(func() {
		time.Sleep(time.Duration(tasks.Config.InitDelay) * time.Second)
		for _, task := range tasks.Scheduled {
			ticker := time.NewTicker(time.Duration(task.Interval) * time.Second)
			go func(task Scheduled) {
				defer ticker.Stop()
				if task.ExecOnInit {
					t := time.Now()
					fmt.Printf("[%s] Scheduled task executed: %s\n", t.Format("15:04:05"), task.Action)
					client.Say(task.TargetChannel, task.Action)
				}
				for {
					select {
					case <-ticker.C:
						t := time.Now()
						client.Say(task.TargetChannel, task.Action)
						fmt.Printf("[%s] Scheduled task executed: %s\n", t.Format("15:04:05"), task.Action)
					}
				}
			}(task)
		}
	})

	client.Join(tasks.Config.Channels...)

	err := client.Connect()
	if err != nil {
		log.Fatal(err)
	}
}

func (a *Reaction) react(m twitchIRC.PrivateMessage, client *twitchIRC.Client) (triggered bool) {
	if strings.ToLower(m.User.Name) != a.TriggerUser {
		return false
	}
	if !strings.HasPrefix(strings.TrimSpace(strings.ToLower(m.Message)), strings.ToLower(a.Trigger)) {
		return false
	}
	go func() {
		t := time.Now()
		fmt.Printf("[%s] Reaction queued: %s\n", t.Format("15:04:05"), a.Reaction)
		time.Sleep(time.Duration(a.DelaySeconds) * time.Second)
		client.Say(a.TargetChannel, a.Reaction)
		t = time.Now()
		fmt.Printf("[%s] %s\n", t.Format("15:04:05"), a.Reaction)
	}()
	return true
}

func readTaskFile() *App {
	content, err := ioutil.ReadFile("taskfile.toml")
	if err != nil {
		log.Fatal(err)
	}
	a := App{}
	toml.Unmarshal(content, &a)
	return &a
}
